package com.example.say;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.example.say.post.PostDAO;
import com.example.say.post.PostVO;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;

public class PostListView extends Activity {
	
	private PostVO vo = new PostVO();
	private PostDAO postDAO = new PostDAO();
	private List<PostVO> postList = new ArrayList<PostVO>();
	private ArrayAdapter<PostVO> postAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.postlistview);
		
		final ListView plist = (ListView)findViewById(R.id.PlistView1);
		
		postList = postDAO.getPostList(vo);
		postAdapter = new ArrayAdapter<PostVO>(this,android.R.layout.simple_list_item_1,postList);
		
		plist.setAdapter(postAdapter);
		
		plist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int pos , long id) {
				// TODO Auto-generated method stub
				
				PostVO selectedPost = postList.get(pos);
				
				Intent intent1 = new Intent(PostListView.this, getPostView.class);
				
				
				
				String pattern = "#.###############";
				DecimalFormat formatx = new DecimalFormat(pattern);
				
				String convertx = formatx.format(selectedPost.getpPositionX());
				BigDecimal xxxx = new BigDecimal(convertx); 
				String converty = formatx.format(selectedPost.getpPositionY());
				BigDecimal yyyy = new BigDecimal(converty); 
				
				postList.get(pos).setPostTitle(""+xxxx+"   "+selectedPost.getpPositionY());
				
				intent1.putExtra("positionX", xxxx.doubleValue());
				intent1.putExtra("positionY",yyyy.doubleValue());
				intent1.putExtra("category", selectedPost.getpCategory());

				//plist.setAdapter(postAdapter);
				startActivity(intent1);
			}
			
		});
	}
}
		
















