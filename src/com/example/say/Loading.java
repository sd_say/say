package com.example.say;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;


public class Loading extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loading);
		
		Handler handler = new Handler(){
			public void handleMessage(Message msg){
				finish();
			}
		};
		
		handler.sendEmptyMessageDelayed(0, 2000);
	}	

}