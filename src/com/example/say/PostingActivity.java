package com.example.say;
// mySpn.getItemAtPosition(mySpn.getSelectedItemPosition());
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;

import com.example.say.post.PostDAO;
import com.example.say.post.PostVO;

import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore.Images;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;

import java.util.Random;

//import com.example.say.MapActivity;

public class PostingActivity extends Activity {

	protected static final int REQ_CODE_PICK_PICTURE = 0;
	Intent intent = new Intent(Intent.ACTION_PICK);
	byte[] byteArray;		
	ImageView camera;
	
	MapActivity mapactivity = new MapActivity();
		
	String post_title= null;
	String post_content = null;
	String user_id = null;
	int post_category ;
	Byte[] post_photo = null;
	double post_positionX = mapactivity.Latitude;
	double post_positionY = mapactivity.Longitude;
	
	final PostVO vo = new PostVO();
	final PostDAO postDAO = new PostDAO();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_posting);
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());

		//img = (ImageView)findViewById(R.id.Posting_photo_ImageView);
		final EditText postTitle = (EditText)findViewById(R.id.Posting_title_editText);
		final EditText postContent = (EditText)findViewById(R.id.Posting_string_editText);	
		final Button post = (Button)findViewById(R.id.Posting_post_Button);
		camera = (ImageView)findViewById(R.id.Posting_photo_ImageView);
		final Spinner s = (Spinner) findViewById(R.id.posting_category_spinner);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.category, android.R.layout.simple_spinner_item);
	    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    s.setAdapter(adapter);

	   	final Tap tp = new Tap();
	   	
	   	camera.setOnClickListener(new OnClickListener(){
	   		//@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent (Intent.ACTION_PICK);
				intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
				intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(intent, REQ_CODE_PICK_PICTURE);
				
			}	   		
	   	});
	   	
		post.setOnClickListener(new OnClickListener(){
		
			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub		
				post_title = postTitle.getText().toString();
				post_content = postContent.getText().toString();
				user_id = tp.getGlobal();
				post_category = s.getSelectedItemPosition();
				
				Toast toastView = null;		
				
				if( !(post_title.isEmpty()) && !(post_content.isEmpty()) ){		
					
					try{
						vo.setPostTitle(post_title);
						vo.setPostContent(post_content);
						vo.setUserID(user_id);
						vo.setpCategory(post_category);
						
						//Random random = new Random();
						vo.setpPositionX(post_positionX);
						vo.setpPositionY(post_positionY);
						
						int result = postDAO.AddPost(vo);
						
						if(result == 1){
							postTitle.setText(null);
							postContent.setText(null);
							toastView = Toast.makeText(getApplicationContext(),
									"글이 셍성되었습니다.", Toast.LENGTH_SHORT);
							toastView.setGravity(Gravity.CENTER, 22, 44);
							toastView.show();
							Intent intent = new Intent(PostingActivity.this, Tap.class);
							startActivity(intent);
						}
						else{}
					}catch(Exception e){
						
					}
				}
				else{
					toastView = Toast.makeText(getApplicationContext(), "글을 만들었습니다!", Toast.LENGTH_SHORT);
        			toastView.setGravity(Gravity.CENTER, 22, 44);
        			toastView.show();
				}
			} //button Listener 종료
			
		});

	}

	@Override 
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == REQ_CODE_PICK_PICTURE){
			if(resultCode == Activity.RESULT_OK){	
				Uri currImgUri = data.getData();
				try {
					Bitmap bmp = Images.Media.getBitmap(getContentResolver(), currImgUri);	
					//ByteArrayOutputStream stream = new ByteArrayOutputStream();				
					//bmp.compress(CompressFormat.JPEG, 100, stream);
					//byteArray = stream.toByteArray();
					camera.setImageBitmap(bmp);
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}			
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.posting, menu);
		return true;
	}

}
