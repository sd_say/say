package com.example.say;

import com.example.say.user.UserDAO;
import com.example.say.user.UserVO;

import android.os.Bundle;
import android.os.StrictMode;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends Activity {

	String user_id = null;
	String user_pw = null;
	String user_email = null;
	UserVO vo = new UserVO();
	UserDAO userDAO = new UserDAO();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		startActivity(new Intent(this, Loading.class));

		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectDiskReads().detectDiskWrites().detectNetwork()
				.penaltyLog().build());

		final ImageButton login_button = (ImageButton) findViewById(R.id.main_login_button);
		final ImageButton join_button = (ImageButton) findViewById(R.id.main_join_button);

		final EditText id = (EditText) findViewById(R.id.main_id_EditText);
		final EditText pw = (EditText) findViewById(R.id.main_pw_EditText);

		login_button.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					user_id = id.getText().toString();
					user_pw = pw.getText().toString();
					Toast toastView = null; // 실행 과정을 보여주는 toast 메세지 생성

					if (!(user_id.isEmpty()) && !(user_pw.isEmpty())) { // 공백이
																		// 있을시
																		// 생성불가

						vo = new UserVO();
						userDAO = new UserDAO();

						vo.setUserID(user_id);
						vo.setUserPW(user_pw);
						vo = userDAO.getUser(vo);

						if (vo != null) {
							Intent intent = new Intent(MainActivity.this,Tap.class); // 두번째 액티비티를 실행하기 위한 인텐트
							intent.putExtra("UserID", vo.getUserID());
							startActivity(intent); // 두번째 액티비티를 실행합니다.
						} else {
							toastView = Toast.makeText(getApplicationContext(),
									"아이디와 비밀번호를 확인하세요.", Toast.LENGTH_SHORT);
							toastView.setGravity(Gravity.CENTER, 22, 44);
							toastView.show();
						}
					}
					else{
						toastView = Toast.makeText(getApplicationContext(),
								"공백이 있습니다.", Toast.LENGTH_SHORT);
						toastView.setGravity(Gravity.CENTER, 22, 44);
						toastView.show();
					}
				} catch (Exception e) {
					Log.e("MainActivity", "Error1 " + e);
				}
			} // button Listener 占쏙옙占쏙옙

		});

		join_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, Join.class);
				startActivity(intent); // 占싸뱄옙째 占쏙옙티占쏙옙티占쏙옙 占쏙옙占쏙옙占쌌니댐옙.
			}

		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
