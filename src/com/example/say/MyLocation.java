package com.example.say;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
 
public class MyLocation{
 
   public static double[] getMyLocation(Context context){
 
        double[] result = null;
        Criteria criteria = new Criteria();
 
        
 
        if(criteria==null){
 
            
 
            
 
            criteria.setAccuracy(Criteria.ACCURACY_COARSE); // 정확도
 
            criteria.setAltitudeRequired(false); // 고도
 
            criteria.setBearingRequired(false); // ..
 
            criteria.setSpeedRequired(false); // 속도
 
        }
 
 
 
        LocationManager locationManager;
 
        
 
        locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
 
 
 
        //true=현재 이용가능한 공급자 제한
 
        String provider = locationManager.getBestProvider(criteria, true);// "gps";
 
 
 
        if (provider == null)
 
            provider = "network";
 
        Location location = locationManager.getLastKnownLocation(provider);
 
        if(location==null){
 
            Intent intent = new Intent(context, MapActivity.class);
 
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
 
            context.startActivity(intent);
 
            ((Activity) context).finish();
 
        }else{
 
            result = new double[]{
 
                location.getLatitude(), location.getLongitude()
 
            };
 
        }
 
        return result; 
 
    }
 
}