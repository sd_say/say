package com.example.say;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import com.example.say.user.UserDAO;
import com.example.say.user.UserVO;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Join extends Activity {

	String user_id = null;
	String user_pw = null;
	String user_email = null;
	UserVO vo = new UserVO();
	UserDAO userDAO = new UserDAO();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.join);

		final EditText id = (EditText) findViewById(R.id.join_id_editText);
		final EditText email = (EditText) findViewById(R.id.join_email_editText);
		final EditText pw = (EditText) findViewById(R.id.join_pw_editText);
		final Button ok_button = (Button) findViewById(R.id.join_ok_button);

		ok_button.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				user_id = id.getText().toString();
				user_pw = pw.getText().toString();
				user_email = email.getText().toString();
				Toast toastView = null; // 실행 과정을 보여주는 toast 메세지 생성

				if (!(user_id.isEmpty()) && !(user_pw.isEmpty()) && !(user_email.isEmpty())){ // 공백이 있을시 생성불가

					vo.setUserID(user_id);
					vo.setUserPW(user_pw);
					vo.setEmail(user_email);

					if (userDAO.AddUser(vo)== 1) {
						toastView = Toast.makeText(getApplicationContext(),
								"ID 생성 성공!", Toast.LENGTH_SHORT);
						toastView.setGravity(Gravity.CENTER, 22, 44);
						toastView.show();
						
						Intent intent = new Intent(Join.this, Tap.class); // 두번째 액티비티를 실행하기 위한 인텐트
						startActivity(intent); // 두번째 액티비티를 실행합니다.
						
						//id.setText(null); // id 생성시 text 초기화
						//pw.setText(null);
						//email.setText(null);
					} else {
						toastView = Toast.makeText(getApplicationContext(),
								"중복된 ID가 있습니다", Toast.LENGTH_SHORT);
						toastView.setGravity(Gravity.CENTER, 22, 44);
						toastView.show();
					}
				} else {
					toastView = Toast.makeText(getApplicationContext(),
							"공백이있습니다!", Toast.LENGTH_SHORT);
					toastView.setGravity(Gravity.CENTER, 22, 44);
					toastView.show();
				}
			} // button Listener 종료

		});

	}

}