package com.example.say;

import java.util.ArrayList;
import java.util.List;

import com.example.say.post.PostDAO;
import com.example.say.post.PostVO;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

//import com.example.say.MapActivity;

public class getPostView extends Activity {
	
	PostVO vo = new PostVO();
	PostVO vo2 = new PostVO();
	PostDAO postDAO = new PostDAO();
	List<PostVO> postList = new ArrayList<PostVO>();

//	MapActivity mapactivity = new MapActivity();
//	double post_positionX = mapactivity.Latitude;
//	double post_positionY = mapactivity.Longitude;
//	int category = mapactivity.category;
	ImageView image = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.getpost);
		
		image = (ImageView)findViewById(R.id.getPost_category_ImageView);
		TextView title = (TextView)findViewById(R.id.getPost_title_EditText);
		final TextView resultLable = (TextView) findViewById(R.id.getPost_content_EditText);
		
		Intent get = getIntent();
		double post_positionX = get.getDoubleExtra("positionX", 0.0);
		double post_positionY = get.getDoubleExtra("positionY", 0.0);
		int category = get.getIntExtra("category",0);

		catory_image(category);
		
	
		vo.setpPositionX(post_positionX);
		vo.setpPositionY(post_positionY);
		
		vo = postDAO.getPost(vo);
		
		if(vo!=null){
			resultLable.setText(vo.getPostContent());
			title.setText(vo.getPostTitle());
		}
		else{
			resultLable.setText("정보가 없습니다.");
		}
		
	}
	void catory_image(int category){			
		Bitmap happy = BitmapFactory.decodeResource(getResources(),
				R.drawable.happy);
		Bitmap sad = BitmapFactory.decodeResource(getResources(),
				R.drawable.sad);
		Bitmap love = BitmapFactory.decodeResource(getResources(),
				R.drawable.love);
		Bitmap friendship = BitmapFactory.decodeResource(getResources(),
				R.drawable.frindship);
		Bitmap horror = BitmapFactory.decodeResource(getResources(),
				R.drawable.horror);
		
		if (category == 0){
			image.setImageBitmap(happy);
		}
		else if(category == 1){
			image.setImageBitmap(sad);
		}
		else if(category == 2){
			image.setImageBitmap(love);
		}
		else if(category == 3){
			image.setImageBitmap(friendship);
		}
		else {
			image.setImageBitmap(horror);
		}
	}
}
