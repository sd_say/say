package com.example.say;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.TabHost;


public class Tap extends TabActivity {

	public static String global = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tap);
		
		Intent get = getIntent();
		global = get.getStringExtra("UserID"); 
		
		Resources res = getResources();
        TabHost tabHost = getTabHost();
        TabHost.TabSpec spec;
        Intent intent;
        
        // �ǿ�Ƽ��Ƽ ������ 0��° ���� ���õǾ����� ���׸� ȸ���ϱ� ���� �ڵ�
        intent = new Intent(this, BlankActivity.class);
        spec = tabHost.newTabSpec("").setIndicator("")
        		.setContent(intent);
        tabHost.addTab(spec);
        tabHost.getTabWidget().getChildTabViewAt(0).setVisibility(View.GONE);
        
        // ù��° ��(1)
        Intent intent3 = new Intent(this, MapActivity.class);
        spec = tabHost.newTabSpec("").setIndicator("",
        		res.getDrawable(R.drawable.tap2))
        		.setContent(intent3);
        tabHost.addTab(spec);

        // �ι�° ��(2)
        intent = new Intent(this, PostingActivity.class);
        spec = tabHost.newTabSpec("").setIndicator("",
        		res.getDrawable(R.drawable.write))
        		.setContent(intent);
        tabHost.addTab(spec);

        // ����° ��(3)
        intent = new Intent(this, PostListView.class);
        spec = tabHost.newTabSpec("").setIndicator("",
        		res.getDrawable(R.drawable.tap3))
        		.setContent(intent);
        tabHost.addTab(spec);
        
     // ����° ��(3)
        intent = new Intent(this, getPostView.class);
        spec = tabHost.newTabSpec("").setIndicator("",
        		res.getDrawable(R.drawable.read))
        		.setContent(intent);
        tabHost.addTab(spec);


        // ����° �� ����
        tabHost.setCurrentTab(1);
		
    }


	public static void setGlobal(String global) {
		Tap.global = global;
	}

	public String getGlobal() {
		// TODO Auto-generated method stub
		return global;
	}

	
}