package com.example.say;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.widget.TextView;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;

@SuppressLint("NewApi")
public class ExteralDB extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {                
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.aa);
        
        TextView tv = (TextView)findViewById(R.id.tv);        
        StringBuilder sb = new StringBuilder();
        
        try{
        	URL url = new URL("http://uranus.smu.ac.kr/~200911258/test2.php");
        	HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        	if(conn!= null){
        		conn.setConnectTimeout(10000);
        		conn.setUseCaches(false);
  
        		
        		if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){
        			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        				     					
        			while (true){
        				String line = br.readLine();
        				if(line == null)
        					break;
        				sb.append(line + "\n");
        				//Log.i("K", line);
        			}
        			br.close();
        		}
        		else{
        			tv.setText("http_not");
        		}
        		conn.disconnect();
        	}        	
        }
        catch(Exception e){
    		tv.setText(e.toString());
    	}
        tv.setText(sb.toString());
        
       
       
       
        String jsonString = sb.toString();        
    
        try{
        	String res = "";
        	JSONArray ja = new JSONArray(jsonString);
        	
        	for (int i = 0; i < ja.length(); i++) {
        		JSONObject jo = ja.getJSONObject(i);
        		
        		res += "id : " +jo.getString("id") + "\npassword : " + jo.getString("password") + "\n";
        	}
        	tv.setText(res);        	
        }
        catch(JSONException e){
        	tv.setText(e.toString());
        }    
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
