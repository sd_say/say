package com.example.say.user;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.say.connection.DBConnection;

import android.os.StrictMode;
import android.util.Log;

public class UserDAO {

	DBConnection db = new DBConnection();
	UserVO user = new UserVO();

	public UserVO getUser(UserVO vo) {
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectDiskReads().detectDiskWrites().detectNetwork()
				.penaltyLog().build());
		StringBuilder sb = new StringBuilder();
		try {
			// url 주소 + 입력값
			URL url = new URL("http://uranus.smu.ac.kr/~200911258/login.php?"
					+ "id=" + URLEncoder.encode(vo.getUserID(), "UTF-8")
					+ "&password=" + URLEncoder.encode(vo.getUserPW(), "UTF-8"));

			HttpURLConnection conn = db.getConnection(url);

			if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				String line = br.readLine();
				sb.append(line);
				br.close();
				Log.d("UserDAO", "getUserInfo");
			} else {
				Log.e("UserDAO", "Error1");
			}
			conn.disconnect();
		} catch (Exception e) {
			Log.e("UserDAO", "Error2 " + e);
		}

		if (!(sb.toString().equals("false"))) {
			String jsonString = sb.toString();
			try {
				JSONObject jo =	new JSONObject(jsonString);

			//	user = new UserVO();
				user.setUserID(jo.getString("id"));
				user.setEmail(jo.getString("email"));
				user.setUserPW(jo.getString("password"));

				Log.d("UserDAO", "setUserVO");
				return user;
			} catch (JSONException e) {
				Log.e("UserDAO", "Error3 " + e);
				return null;
			}
		} else {
			Log.d("UserDAO", "notFoundUser");
			return null;
		}
	}

	public int AddUser(UserVO vo) {
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectDiskReads().detectDiskWrites().detectNetwork()
				.penaltyLog().build());
		StringBuilder sb = new StringBuilder();
		try {
			// url 주소 + 입력값
			URL url = new URL("http://uranus.smu.ac.kr/~200911258/join.php?"
					+ "id=" + URLEncoder.encode(vo.getUserID(), "UTF-8")
					+ "&password=" + URLEncoder.encode(vo.getUserPW(), "UTF-8")
					+ "&email=" + URLEncoder.encode(vo.getEmail(), "UTF-8"));

			HttpURLConnection conn = db.getConnection(url);

			if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				String line = br.readLine();
				sb.append(line);
				br.close();
				conn.disconnect();
				if(sb.toString().equals("true")){
					Log.d("UserDAO", "addUserInfo");
					return 1;
				}
				else
					return 0;
			} else {
				conn.disconnect();
				Log.e("UserDAO", "Error4");
				return 0;
			}
		} catch (Exception e) {
			Log.e("UserDAO", "Error5" + e);
			return 0;
		}
	}

}
