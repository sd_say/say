package com.example.say.user;

public class UserVO {

	private	String UserName;
	private String Email;
	private String UserID;
	private String UserPW;
	
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	public String getUserPW() {
		return UserPW;
	}
	public void setUserPW(String userPW) {
		UserPW = userPW;
	}
	
	@Override
	public String toString() {
		return "UserVO [UserName=" + UserName + ", Email=" + Email
				+ ", UserID=" + UserID + ", UserPW=" + UserPW + "]";
	}
}