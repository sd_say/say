package com.example.say;

import java.util.ArrayList;
import java.util.List;

import com.example.say.post.PostDAO;
import com.example.say.post.PostVO;
import com.google.android.gms.maps.GoogleMap;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends FragmentActivity {

	private GoogleMap gmap;
	private LatLng position;
	private LatLng my_location;
	private double[] my_gps;
	private LatLng aa;
	public static double Latitude = 0;
	public static double Longitude = 0;
	public int category = 0;
	
	

	@Override
	protected void onCreate(Bundle icicle) {

		super.onCreate(icicle);

		setContentView(R.layout.map);

		final PostVO vo = new PostVO();
		List<PostVO> list = new ArrayList<PostVO>();
		final PostDAO postDAO = new PostDAO();


		GooglePlayServicesUtil.isGooglePlayServicesAvailable(MapActivity.this);

		my_gps = MyLocation.getMyLocation(MapActivity.this);
		my_location = new LatLng(my_gps[0], my_gps[1]);

		gmap = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.fragment1)).getMap(); // 액티비티에서 맵을 가져온다.
		gmap.setMyLocationEnabled(true);

		gmap.addMarker(new MarkerOptions().position(my_location));

		// maker();

		gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(my_location, 15));// position의
																			// 위치로
																			// 이동시키고
																			// 15배
																			// 줌인
																			// 시킨다.

		list = postDAO.getPostList(vo);

		if (list.size() != 0) {
			for (int i = 0; i < list.size(); i++) {
				position = new LatLng(list.get(i).getpPositionX(), list.get(i)
						.getpPositionY());
				category = list.get(i).getpCategory();
				
				
				//gmap.addMarker(new MarkerOptions().position(position).icon(
				//		BitmapDescriptorFactory.fromBitmap(happy)));
				marker(category,position);
			}
		}

		gmap.setOnMapLongClickListener(new OnMapLongClickListener() {
			@Override
			public void onMapLongClick(LatLng latLng) {
				// MarkerOptions markerOptions = new MarkerOptions();
				// markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.logo));
				// markerOptions.position(latLng).title("여기로 포스팅").snippet("바로여기");
				aa = latLng;
				Latitude = aa.latitude;
				Longitude = aa.longitude;
	
				gmap.clear();
				gmap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
				// gmap.addMarker(markerOptions);

				Intent intent1 = (new Intent(MapActivity.this,
						PostingActivity.class));
				startActivity(intent1);

			}
		});

		gmap.setOnMarkerClickListener(new OnMarkerClickListener() {

			@Override
			public boolean onMarkerClick(Marker marker) {
				// TODO Auto-generated method stub

				LatLng lt = marker.getPosition();
				
				Latitude = lt.latitude;
				Longitude = lt.longitude;
				
				vo.setpPositionX(Latitude);
				vo.setpPositionY(Longitude);
				
				category = postDAO.getPost(vo).getpCategory();

				Intent intent1 = (new Intent(MapActivity.this,
						getPostView.class));
				
				intent1.putExtra("positionX", Latitude);
				intent1.putExtra("positionY", Longitude);
				intent1.putExtra("category", category);
				
				startActivity(intent1);

				// Toast.makeText(MapActivity.this, "Oh my god!!",
				// Toast.LENGTH_SHORT).show();
				return false;
			}
		});

		gmap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
	}

	void marker(int category, LatLng position) {
		
		Bitmap happy = BitmapFactory.decodeResource(getResources(),
				R.drawable.happy);
		Bitmap sad = BitmapFactory.decodeResource(getResources(),
				R.drawable.sad);
		Bitmap love = BitmapFactory.decodeResource(getResources(),
				R.drawable.love);
		Bitmap friendship = BitmapFactory.decodeResource(getResources(),
				R.drawable.frindship);
		Bitmap horror = BitmapFactory.decodeResource(getResources(),
				R.drawable.horror);
		

		if (category == 0){
			gmap.addMarker(new MarkerOptions().position(position).icon(
					BitmapDescriptorFactory.fromBitmap(happy)));
		}
		else if(category == 1){
			gmap.addMarker(new MarkerOptions().position(position).icon(
					BitmapDescriptorFactory.fromBitmap(sad)));
		}
		else if(category == 2){
			gmap.addMarker(new MarkerOptions().position(position).icon(
					BitmapDescriptorFactory.fromBitmap(love)));
		}
		else if(category == 3){
			gmap.addMarker(new MarkerOptions().position(position).icon(
					BitmapDescriptorFactory.fromBitmap(friendship)));
		}
		else {
			gmap.addMarker(new MarkerOptions().position(position).icon(
					BitmapDescriptorFactory.fromBitmap(horror)));
		}

		

	}

}
