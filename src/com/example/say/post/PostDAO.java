package com.example.say.post;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.spec.EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.StrictMode;
import android.util.Log;

import com.example.say.connection.DBConnection;
import com.example.say.user.UserVO;

public class PostDAO {

	DBConnection db = new DBConnection();
	PostVO post = new PostVO();
	List<PostVO> list;
	URL url;

	public PostVO getPost(PostVO vo) {
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectDiskReads().detectDiskWrites().detectNetwork()
				.penaltyLog().build());
		StringBuilder sb = new StringBuilder();
		list = new ArrayList<PostVO>();
		
		try {
			// 1. Position 이용 하여 글 한개 가져오기
			// url 주소 + 입력값
//			if(vo.getPostID() != null )
//			{
//			url = new URL("http://uranus.smu.ac.kr/~200911258/searchPostID.php?"
//					+ "postID="+vo.getPostID());
//			}
//			else
//			{
				url = new URL("http://uranus.smu.ac.kr/~200911258/searchXY.php?"
						+ "x="+vo.getpPositionX()+"&y="+vo.getpPositionY());
//			}
			
			HttpURLConnection conn = db.getConnection(url);

			if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				String line = br.readLine();
				sb.append(line);
				br.close();
				Log.d("PostDAO", "getPostInfo");
			} else {
				Log.e("PostDAO", "Error1");
			}
			conn.disconnect();
		} catch (Exception e) {
			Log.e("PostDAO", "Error2 " + e);
		}
		if (!(sb.toString().equals("false"))) {
			String jsonString = sb.toString();
			try {
				JSONArray ja = new JSONArray(jsonString);
//				JSONObject jo = new JSONObject(jsonString);
				for (int i = 0; i < ja.length(); i++) {
					
					JSONObject jo = ja.getJSONObject(i);

					post = new PostVO();
					post.setUserID(jo.getString("id"));
					post.setPostContent(jo.getString("content"));
					post.setpCategory(jo.getInt("category"));
					post.setPostTitle(jo.getString("title"));
					post.setpPositionX(jo.getDouble("positionX"));
					post.setpPositionY(jo.getDouble("positionY"));
					post.setPostID(jo.getString("post_num"));
					
				list.add(post);
								}
				Log.d("PostDAO", "setPostList");
			//	return post;
				return list.get(0);
			} catch (JSONException e) {
				Log.e("PostDAO", "Error3 " + e);
				return null;
			}
		} else {
			Log.d("PostDAO", "notFoundPost");
			return null;
		}
	}

	public int AddPost(PostVO vo) {
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectDiskReads().detectDiskWrites().detectNetwork()
				.penaltyLog().build());
		StringBuilder sb = new StringBuilder();
		try {
			// url 주소 + 입력값
			url = new URL("http://uranus.smu.ac.kr/~200911258/posting.php?"
					+ "title=" + URLEncoder.encode(vo.getPostTitle(), "UTF-8")
					+ "&content="
					+ URLEncoder.encode(vo.getPostContent(), "UTF-8")
					+ "&category=" + vo.getpCategory() + "&id="
					+ URLEncoder.encode(vo.getUserID(), "UTF-8")
					+ "&positionX=" + vo.getpPositionX() + "&positionY="
					+ vo.getpPositionY());

			HttpURLConnection conn = db.getConnection(url);

			if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				String line = br.readLine();
				sb.append(line);
				br.close();
				conn.disconnect();
				if (sb.toString().equals("true")) {
					Log.d("PostDAO", "addPostInfo");
					return 1;
				} else
					return 0;
			} else {
				conn.disconnect();
				Log.e("PostDAO", "Error4");
				return 0;
			}
		} catch (Exception e) {
			Log.e("PostDAO", "Error5" + e);
			return 0;
		}
	}

	// /////////////////////

	public List<PostVO> getPostList(PostVO vo) {

		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectDiskReads().detectDiskWrites().detectNetwork()
				.penaltyLog().build());
		StringBuilder sb = new StringBuilder();

		list = new ArrayList<PostVO>();

		try {
			// 1. Position 이용 하여 글 한개 가져오기
			// url 주소 + 입력값
			url = new URL("http://uranus.smu.ac.kr/~200911258/sorting.php");

			HttpURLConnection conn = db.getConnection(url);

			if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				String line = br.readLine();
				sb.append(line);
				br.close();
				Log.d("PostDAO", "getPostList");
			} else {
				Log.e("PostDAO", "Error1");
			}
			conn.disconnect();
		} catch (Exception e) {
			Log.e("PostDAO", "Error2 " + e);
		}

		if (!(sb.toString().equals("false")) & sb.toString() != null) {
			String jsonString = sb.toString();
			try {
				JSONArray ja = new JSONArray(jsonString);

				for (int i = 0; i < ja.length(); i++) {
					
					JSONObject jo = ja.getJSONObject(i);
					
					post = new PostVO();
					post.setUserID(jo.getString("id"));
					post.setPostContent(jo.getString("content"));
					post.setpCategory(jo.getInt("category"));
					post.setPostTitle(jo.getString("title"));
					post.setpPositionX(jo.getDouble("positionX"));
					post.setpPositionY(jo.getDouble("positionY"));
					post.setPostID(jo.getString("post_num"));
					
					list.add(post);
				}
				Log.d("PostDAO", "setPostList");
				return list;
			} catch (JSONException e) {
				Log.e("PostDAO", "Error3 " + e);
				return null;
			}
		} else {
			Log.d("PostDAO", "notFoundPost");
			return null;
		}
	}

}
