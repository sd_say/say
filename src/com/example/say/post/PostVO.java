package com.example.say.post;

import java.util.Arrays;

public class PostVO {
	
	private String postTitle;
	private String postContent;
	private String postID;
	private String UserID;
	private byte[] pImage;
	private int pCategory = 0;
	private double pPositionX = 0.0f;
	private double pPositionY = 0.0f;
	public String getPostTitle() {
		return postTitle;
	}
	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}
	public String getPostContent() {
		return postContent;
	}
	public void setPostContent(String postContent) {
		this.postContent = postContent;
	}
	public String getPostID() {
		return postID;
	}
	public void setPostID(String postID) {
		this.postID = postID;
	}
	public byte[] getpImage() {
		return pImage;
	}
	public void setpImage(byte[] bs) {
		this.pImage = bs;
	}
	public int getpCategory() {
		return pCategory;
	}
	public void setpCategory(int pCategory) {
		this.pCategory = pCategory;
	}
	public double getpPositionX() {
		return pPositionX;
	}
	public void setpPositionX(double pPositionX) {
		this.pPositionX = pPositionX;
	}
	public double getpPositionY() {
		return pPositionY;
	}
	public void setpPositionY(double pPositionY) {
		this.pPositionY = pPositionY;
	}
	
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	@Override
	public String toString() {
		return getPostTitle();
	}
	
	

}
